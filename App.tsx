import 'react-native-gesture-handler';
import React from 'react'
import {View, Text} from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
// import { Navigator } from './src/navigator/ExplorerNavigator';

import { BottomTabs } from './src/navigator/BottomTabs';
import { StackNavigator } from './src/navigator/StackNavigator';

const App = () => {
  return (
    <NavigationContainer>
        {/* <Navigator /> */}
        <StackNavigator />
    </NavigationContainer>
  )
}
export default App;

