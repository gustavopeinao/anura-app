export interface AnuroItem {
    id: number;
    name: string;
    family: string;
    uri: string;
}