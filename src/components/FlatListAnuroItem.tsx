import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import Icon from "react-native-vector-icons/Ionicons";
import { AnuroItem } from '../interfaces';

interface Props {
  anuroItem: AnuroItem
}

const FlatListAnuroItem = ({ anuroItem }: Props) => {
  const navigator = useNavigation()

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={() => navigator.navigate('AnuroScreen', anuroItem)}
    >
      <View style={styles.target}>
        <Image
          source={anuroItem.uri}
          style={styles.image}
        />
        {/* {getImage(anuroItem)} */}
        <View style={styles.textContainer}>
          <Text style={styles.targetName}>{anuroItem.name}</Text>
          <Text style={styles.targetFamily}>{anuroItem.family}</Text>
        </View>

        <View style={{ flex: 1 }}></View>
        <View style={styles.iconContainer}>
          <Icon
            name='chevron-forward-outline'
            color='gray'
            size={23}
          />
        </View>

      </View>
    </TouchableOpacity>
  )
}
export default FlatListAnuroItem;
const styles = StyleSheet.create({
  target: {
    flexDirection: 'row'
  },
  textContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    marginLeft: 20
  },
  iconContainer: {
    justifyContent: 'center',
  },
  targetName: {
    fontSize: 20,
    fontWeight: 'bold',
    color:'gray'
  },
  targetFamily: {
    fontSize: 15,
    color:'gray'
  },
  image: {
    width: 90,
    height: 90,
    borderRadius: 45
  },
})
