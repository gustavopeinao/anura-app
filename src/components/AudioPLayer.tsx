import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity, Button, StyleSheet } from 'react-native';
import TrackPlayer from 'react-native-track-player';
import { useProgress } from 'react-native-track-player/lib/hooks';
import Slider from '@react-native-community/slider';
import Icon from "react-native-vector-icons/Ionicons";

//function to initialize the Track Player 
const trackPlayerInit = async () => {
    await TrackPlayer.setupPlayer();
    await TrackPlayer.add({
        id: '1',
        //url: 'https://audio-previews.elements.envatousercontent.com/files/103682271/preview.mp3',
        url: require('../audios/frog-song.mp3'),
        // type: 'default',
        title: 'My Title',
        album: 'My Album',
        artist: 'Rohan Bhatia',
        artwork: 'https://picsum.photos/100',
    });
    return true;
};

export const AudioPLayer = () => {
    //state to manage whether track player is initialized or not
    const [isTrackPlayerInit, setIsTrackPlayerInit] = useState(false);
    const [isPlaying, setIsPlaying] = useState(false);

    //the value of the slider should be between 0 and 1
    const [sliderValue, setSliderValue] = useState(0);
    //flag to check whether the use is sliding the seekbar or not
    const [isSeeking, setIsSeeking] = useState(false);

    //useTrackPlayerProgress is a hook which provides the current position and duration of the track player.
    //These values will update every 250ms 
    const { position, duration } = useProgress(250);


    //initialize the TrackPlayer when the App component is mounted
    useEffect(() => {
        const startPlayer = async () => {
            let isInit = await trackPlayerInit();
            setIsTrackPlayerInit(isInit);
        }
        startPlayer();
    }, []);

    //this hook updates the value of the slider whenever the current position of the song changes
    useEffect(() => {
        if (!isSeeking && position && duration) {
            setSliderValue(position / duration);
        }
    }, [position, duration]);

    //start playing the TrackPlayer when the button is pressed 
    const onButtonPressed = () => {
        if (!isPlaying) {
            TrackPlayer.play();
            setIsPlaying(true);
        } else {
            TrackPlayer.pause();
            setIsPlaying(false);
        }
    }

    //this function is called when the user starts to slide the seekbar
    const slidingStarted = () => {
        setIsSeeking(true);
    };
    //this function is called when the user stops sliding the seekbar
    const slidingCompleted = async value => {
        await TrackPlayer.seekTo(value * duration);
        setSliderValue(value);
        setIsSeeking(false);
    };

    return (
        <View style={styles.container}>
            <TouchableOpacity
                onPress={onButtonPressed}
                style={{ backgroundColor: 'red' }}
            >
                {isPlaying ?
                    <Icon
                        name='pause-circle-outline'
                        color='gray'
                        size={35}
                    /> :
                    <Icon
                        name='play-circle-outline'
                        color='gray'
                        size={35}
                    />
                }

            </TouchableOpacity>
            {/* <Button
                title={isPlaying ? 'Pause' : 'Play'}
                onPress={onButtonPressed}
                disabled={!isTrackPlayerInit}
            /> */}
            {/* defining our slider here */}
            
                <Slider
                    style={{ width: '100%', height: 40, backgroundColor: 'blue' }}
                    minimumValue={0}
                    maximumValue={1}
                    value={sliderValue}
                    minimumTrackTintColor="#111000"
                    maximumTrackTintColor="#000000"
                    onSlidingStart={slidingStarted}
                    onSlidingComplete={slidingCompleted}
                />
            

        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-start',
    }
})
