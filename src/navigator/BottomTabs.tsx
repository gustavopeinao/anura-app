import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Icon from "react-native-vector-icons/Ionicons";

import { ExplorerNavigator } from "../navigator/ExplorerNavigator";
import { IndicationsScreen } from "../screens/IndicationsScreen";
// import { ExplorerScreen } from "../screens/AnuroScreen";
import { ToRegisterScreen } from "../screens/ToRegisterScreen";
import { ProfileScreen } from '../screens/ProfileScreen';

const Tab = createMaterialBottomTabNavigator();

export const BottomTabs = () => {
    return (
        <Tab.Navigator
            initialRouteName="Explorer"
            barStyle={{ backgroundColor: '#02c853' }}
            screenOptions={({ route }) => ({
                headerShown:true,
                tabBarIcon: () => {
                    switch (route.name) {
                        case 'Indications':
                            return <Image source={require("../assests/icons/indicationsIcon.png")} style={{ width: 23, height: 23, tintColor: 'white', resizeMode: 'contain' }} />
                        case 'Explorer':
                            return <Image source={require("../assests/icons/explorerIcon.png")} style={{ width: 40, height: 22, tintColor: 'white', resizeMode: 'contain' }} />
                        case 'ToRegister':
                            return <Image source={require("../assests/icons/frogIcon.png")} style={{ width: 27, height: 27, tintColor: 'white', resizeMode: 'contain' }} />
                        case 'Profile':
                            return (
                                <View style={{ width: 32, height: 32, marginBottom: 10, bottom: 5 }}>
                                    <Icon name='person-circle-outline' color='white' size={32} />
                                </View>)
                    }
                }
            })}
        >
            <Tab.Screen name="Indications" options={{ title: 'Indicaciones' }} component={IndicationsScreen} />
            <Tab.Screen name="Explorer" options={{ title: 'Explorar' }} component={ExplorerNavigator} />
            <Tab.Screen name="ToRegister" options={{ title: 'Registrar' }} component={ToRegisterScreen} />
            <Tab.Screen name="Profile" options={{ title: 'Mi perfil' }} component={ProfileScreen} />
        </Tab.Navigator>
    );
}