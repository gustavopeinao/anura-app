import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { BottomTabs } from './BottomTabs'
import { ExplorerScreen } from "../screens/ExplorerScreen";
import { AnuroScreen } from "../screens/AnuroScreen";

const Stack = createStackNavigator();

export const ExplorerNavigator = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Screen name="ExplorerScreen" component={ExplorerScreen} />
            <Stack.Screen name="AnuroScreen" component={AnuroScreen} />
        </Stack.Navigator>
    );
}