import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { SplashScreen } from "../screens/SplashScreen";
import { BottomTabs } from "../navigator/BottomTabs";

const Stack = createStackNavigator();

export const StackNavigator = () => {
    return (
        <Stack.Navigator
        initialRouteName="SplashScreen"
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Screen name="SplashScreen" component={SplashScreen} />
            <Stack.Screen name="BottomTabs" component={BottomTabs} />
        </Stack.Navigator>
    );
}
