import React, { useEffect } from 'react';
import { FlatList, View, Text, StyleSheet } from 'react-native';
import { anuros } from '../constants';
import FlatListAnuroItem from '../components/FlatListAnuroItem';
import { useNavigation } from '@react-navigation/core';

export const ExplorerScreen = () => {
    const navigator = useNavigation();

    useEffect(() => {
        // navigator.setOptions({
        //     headerShown:true,
        //     title:'Home'
        // })
    }, [])

    const itemseparator = () => {
        return (
            <View style={styles.separator} />
        )
    }
    return (
        <View style={styles.container}>
            <Text>Explorer screen</Text>
            <FlatList
                data={anuros}
                renderItem={({ item }) => <FlatListAnuroItem anuroItem={item} />}
                keyExtractor={(item) => item.id}
                ItemSeparatorComponent={itemseparator}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 20
    },
    separator: {
        borderBottomWidth: 1,
        borderColor: 'gray',
        opacity: 0.4,
        marginVertical: 8
    }
})

