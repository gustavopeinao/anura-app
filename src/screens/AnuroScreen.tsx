import React, { useEffect } from 'react'
import { StackScreenProps } from '@react-navigation/stack';
import { View, Text, Button, Image, StyleSheet, Dimensions, ScrollView } from 'react-native';
import Icon from "react-native-vector-icons/Ionicons";
import { AnuroItem } from '../interfaces';
import { AudioPLayer } from '../components/AudioPLayer';

import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";


const screenHeght = Dimensions.get('screen').height;

interface Props extends StackScreenProps<any, any> { };

export const AnuroScreen = ({ route, navigation }: Props, anuro: any) => {
  const params = route.params as AnuroItem
  useEffect(() => {
    console.log('anuro', params)
  }, [])

  return (
    <ScrollView style={styles.imageContainer}>
      <Image
        source={params.uri}
        style={styles.image}
      />
      <View>
        <Text style={styles.text}>{params.name}</Text>
        <Text style={styles.text}>Familia: {params.family}</Text>

        <Text style={styles.text}>Grado de amenaza</Text>
        <View style={styles.circleContainer}>
          <View style={styles.circle}></View>
          <View style={styles.circle}></View>
          <View style={styles.circle}></View>
        </View>

        <Text style={styles.text}>Periodo de actividad</Text>
        <LineChart
          data={{
            labels: ["En", "Fe", "Ma", "Ab", "My", "Ju", 'Jul', 'Ag', 'Sep', 'Oc', 'Nov', 'Dic'],
            datasets: [
              {
                data: [
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100
                ]
              }
            ]
          }}
          width={Dimensions.get("window").width} // from react-native
          height={220}
          yAxisLabel="$"
          yAxisSuffix="k"
          yAxisInterval={1} // optional, defaults to 1
          chartConfig={{
            backgroundColor: "#02c853",
            backgroundGradientFrom: "#02c853",
            backgroundGradientTo: "#02c853",
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16
            },
            propsForDots: {
              r: "6",
              strokeWidth: "2",
              stroke: "#ffa726"
            }
          }}
          bezier
          style={{
            marginVertical: 8,
            borderRadius: 16
          }}
        />

        <Text style={styles.text}>Audio</Text>
        <AudioPLayer />

      </View>

      {/* <Button
        title='Volver'
        onPress={() => navigation.pop()}
      />
      <Text>AnuroScreen</Text> */}

    </ScrollView>
  )
}
const styles = StyleSheet.create({
  imageContainer: {
    width: '100%',
    height: screenHeght * 0.5,
    //backgroundColor: 'red'
  },
  text: {
    color: 'gray'
  },
  image: {
    width: '100%',
    height: undefined,
    aspectRatio: 1,
  },
  circleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  circle: {
    width: 26,
    height: 26,
    borderRadius: 13,
    borderWidth: 1,
    borderColor: 'red'
  }
})

