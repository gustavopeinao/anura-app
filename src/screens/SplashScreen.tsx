import React, { useEffect } from 'react';
import { Text, StyleSheet, View, ActivityIndicator } from 'react-native';
import { useNavigation } from '@react-navigation/core';

export const SplashScreen = () => {
    const navigator = useNavigation()

    useEffect(() => {
        // setTimeout(() => {
        //     navigator.navigate('BottomTabs')
        // }, 2000);

    }, [])


    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row'}}>
                <Text style={styles.text1}>Anura</Text>
                <Text style={styles.text2}>App</Text>
            </View>

            <ActivityIndicator
                color='white'
            />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#02c853'
    },
    text1: {
        fontSize: 25,
        color: 'white'
    },
    text2: {
        fontSize: 25,
        color: 'black'
    }
})
