export declare type dimensions = {
    width: number
    height: number
}
export declare type ResponsiveSize = {
    size?: number
    tablet?: number
    small?: number
    phone?: number
}