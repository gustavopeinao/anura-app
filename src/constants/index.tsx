import { Dimensions, Platform } from 'react-native'
// import { RFValue } from 'react-native-responsive-fontsize'
// import { isTablet } from 'react-native-device-info'
// import {
//     heightPercentageToDP as hp,
//     widthPercentageToDP as wp,
// } from 'react-native-responsive-screen'
import { dimensions, ResponsiveSize } from './types'

export const window: dimensions = {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
}

export const screen: dimensions = {
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
}

export const isSmallDevice = Dimensions.get('window').width < 375
export const systemBlueColor = 'rgb(10, 123, 255)'

export const navigation = {
    background: {
        drawer: '#012E4F',
        bottom: '#031B2E',
        active: '#053962',
    },
    label: {
        active: '#fff',
        inactive: '#fff',
    },
}

export const colors = {
    white: '#fff',
    refreshControl: Platform.OS === 'android' ? '#000' : 'silver',
}

// export const setResponsiveSize = ({
//     size,
//     phone,
//     small,
//     tablet,
// }: ResponsiveSize) => {
//     return isSmallDevice
//         ? RFValue(small || size || 17)
//         : isTablet()
//         ? RFValue(tablet || size || 10)
//         : RFValue(phone || size || 12)
// }

// export const responsiveHeight = ({
//     small,
//     size,
//     tablet,
//     phone,
// }: ResponsiveSize) => {
//     return isSmallDevice
//         ? hp(small || size || 17)
//         : isTablet()
//         ? hp(tablet || size || 10)
//         : hp(phone || size || 12)
// }

// export const responsiveWidth = ({
//     small,
//     size,
//     tablet,
//     phone,
// }: ResponsiveSize) => {
//     return isSmallDevice
//         ? wp(small || size || 17)
//         : isTablet()
//         ? wp(tablet || size || 10)
//         : wp(phone || size || 12)
// }

export const anuros = [
    {
        id:1,
        name:'Gastrotheca',
        family:'Microhylidae',
        uri:require('../assests/images/anuro.jpeg')
    },
    {
        id:2,
        name:'Melanophryniscus',
        family:'Microhylidae',
        uri:require('../assests/images/anuro2.png')
    },
    {
        id:3,
        name:'Oreobates',
        family:'Microhylidae',
        uri:require('../assests/images/anuro3.jpeg')
    },
    {
        id:4,
        name:'Phyllomedusa',
        family:'Microhylidae',
        uri:require('../assests/images/anuro4.jpg')
    },
    {
        id:5,
        name:'Hypsiboas',
        family:'Microhylidae',
        uri:require('../assests/images/anuro5.jpg')
    },
    {
        id:6,
        name:'Rhinella',
        family:'Microhylidae',
        uri:require('../assests/images/anuro6.jpeg')
    },
    {
        id:7,
        name:'Odontophrynus',
        family:'Microhylidae',
        uri:require('../assests/images/anuro7.jpeg')
    },
    {
        id:8,
        name:'Physalaemus',
        family:'Microhylidae',
        uri:require('../assests/images/anuro8.jpg')
    }
]